package it.feio.utils.obj;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;


/**
 * Class rappresenting a generic object obtained as record from a database query. Many values types are suported to respect as much as possible the original SQL types.
 * 
 * @author Federico Iosue - federico.iosue@gmail.com
 * 
 */
/**
 * @author 17000026 (Federico Iosue - Sistemi&Servizi) 03/ott/2013
 * 
 */
public class Entity {

	private LinkedHashMap<String, Object> attributes;

	Entity(LinkedHashMap<String, Object> attributes) {
		this.attributes = attributes;
	}

	/**
	 * Static method to build a list of Entity objects given an SQL ResultSet. By default database column names are used to name the attributes, this can be changed with appropriate method.
	 * 
	 * @param rs
	 *              ResultSet
	 * @return List of Entity
	 * @throws SQLException
	 */
	public static List<Entity> getList(ResultSet rs) throws SQLException {
		List<Entity> list = new ArrayList<Entity>();
		List<String> attributesList = new ArrayList<String>();
		LinkedHashMap<String, Object> attributes;

		// Retrieval of metadata
		ResultSetMetaData rsmd = rs.getMetaData();
		for (int i = 1; i < rsmd.getColumnCount() + 1; i++) {
			attributesList.add(rsmd.getColumnName(i));
		}

		// Cylcing resultset records
		String key = null;
		Object val = null;
		while (rs.next()) {
			attributes = new LinkedHashMap<String, Object>();

			// Cycling record columns trying to keep the type from SQL to Java
			for (int i = 0; i < attributesList.size(); i++) {
				key = attributesList.get(i);
				switch (rsmd.getColumnType(i + 1)) {
					case Types.NUMERIC:
						val = rs.getInt(key);
						break;
					case Types.BOOLEAN:
						val = rs.getBoolean(key);
						break;
					case Types.FLOAT:
						val = rs.getFloat(key);
						break;
					case Types.TIMESTAMP:
						val = rs.getTimestamp(key);
						break;
					default:
						val = rs.getString(key);
						break;
				}

				// Updating current Entity (record) attributes setting map key in lowecase (Oracle gives by default columns in uppercase)
				attributes.put(key.toLowerCase(), val);
			}
			// Updating list of Entity objects
			list.add(new Entity(attributes));
		}
		return list;
	}


	/**
	 * Gets Entity attributes ordered full map
	 * 
	 * @return Ordered map
	 */
	public LinkedHashMap<String, Object> getAttributes() {
		return attributes;
	}


	/**
	 * Sets Entity attribute map (hardly used)
	 * 
	 * @param attributes
	 *              Ordered map to by associated with entity
	 */
	public void setAttributes(LinkedHashMap<String, Object> attributes) {
		this.attributes = attributes;
	}


	/**
	 * Get entity specific attribute by map key
	 * 
	 * @param key
	 *              Attribute name to be retrieved
	 * @return Entity attribute value
	 */
	public Object getAttribute(String key) {
		return attributes.get(key);
	}


	/**
	 * Get entity specific attribute by map key index
	 * 
	 * @param index
	 *              Attribute index in attribute's ordered map (the order is the same that in query)
	 * @return Entity attribute value
	 */
	public Object getAttribute(int index) {
		String key = (String) attributes.keySet().toArray()[index];
		return attributes.get(key);
	}


}
